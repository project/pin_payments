
PIN PAYMENTS LITE
=================
o Simplest realisation for both site builder and customer of a small store,
  withouth the need for a bulky suite of commerce moduls.
o Add a "Buy" button to any regular content page. No shopping cart required.
o Ideal when your product is in an email, voucher or file attachment.
o If no shipping is required, purchaser's address does not need to be collected.
o Seamlessly delegates to Pin Payments to deal with credit cards, currencies and
  security. Purchaser does not leave your site.
o No merchant account required, receive funds straight into your bank account.


INSTALLATION & CONFIGURATION
============================
You need to sign up with http://pin.net.au. You'll receive a publishable and
a secret key that you need to enter at on the module configuration page,
admin/config/system/pin-payments.
The module makes a purchase form available as a block. The block can be
configured with product descriptions and prices.

AUTHORS
=======
Rik de Boer, Melbourne, Australia.
